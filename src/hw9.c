/*
Homework #9
name: Kevin Young
*/
#include "hw9.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include <netinet/in.h>

#ifdef CSE220
  #define struct_info(cursor) print_list(cursor)
  #define memory_used(r_count, i_count, j_count, it_size, i_size, total_mem) \
    printf("\nCSE220: There is %zu r-type, " \
    "%zu i-type, and %zu j_type nodes.\nCSE220: The InstrType node takes " \
    "up %zu bytes in memory. \nCSE220: The Instr node takes up %zu bytes " \
  "in memory. \nCSE220: Your program allocated %zu nodes that take up " \
    "%zu bytes in memory\n\n", r_count, i_count, j_count, it_size, i_size, \
     (r_count + i_count + j_count + 3), total_mem)
#else
  #define struct_info(cursor)
  #define memory_used(r_count, i_count, j_count, it_size, i_size, total_mem)
#endif

#define USAGE(name) fprintf(stderr, \
"Usage: %s [-h] [-m INSTRUCTION_MAPPING] -i BIN_FILE -o OUTPUT_FILE\n\n" \
"-h 	                  Displays this help menu\n\n" \
"-m INSTRUCTION_MAPPING    File that contains the instruction mapping.\n" \
"		          If this option is not provided it defaults to\n" \
"		          instruction_mapping.txt\n\n" \
"-i BIN_FILE               This should be a binary file that contains the .text\n" \
"			  section of a Mars220 MIPS binary. \n\n" \
"-o OUTPUT_FILE            This can be any file on the system or \"-\" which \n" \
"			  specifies stdout. \n", (name))

#define BUFFER_SIZE 4
#define FILE_BUFFER 256

void close_files(FILE* ifp, FILE* ofp, FILE* instr_p, int stdoutFlag, int mFlag, char* INPUT_FILE, char* OUTPUT_FILE, int deleteOutFlag); // behold!
int files_duplicate(char* input, char* output, char* instr, int stdoutFlag);
int file_exists(char* filename);
int file_success(FILE* ifp, FILE* ofp, FILE* instr_P, int stdoutFlag);
int files_empty(char* input, char* instr);
int check_bom(FILE* binFile);
off_t check_byte_count(char* binFile);
int read_instr(FILE* instr, FILE* ofp, FILE* bin_file, size_t byte_count, int stdoutFlag, int LEfile, int BEmachine);
void free_list(InstrType* head);
void initialize_list(InstrType* head, InstrType* iType, InstrType* jType);
int print_node_data(Instr* r_head, Instr* i_head, Instr* j_head);
int list_insert(InstrType* list, Instr* new_node, char* mnemonic);
char* get_reg_name(int numeric);
char get_type(char instr_str[]);
char* get_mnemonic(char* mnemonic, char instr_str[], int m_length);
int get_mnemonic_length(char instr_str[]);
int write_rType(unsigned int num, unsigned int opcode, int stdoutFlag, InstrType* head, Instr* cursor, FILE* ofp);
int write_iType(unsigned int num, unsigned int opcode, int stdoutFlag, InstrType* head, Instr* cursor, FILE* ofp);
int write_jType(unsigned int num, unsigned int opcode, int stdoutFlag, InstrType* head, Instr* cursor, FILE* ofp);
int pretty_print(FILE* file, Instr* node, int imm, int shamt, int rd, int rs, int rt);
void print_list(InstrType* cursor);
int valid_hex(char* hex_str);
int valid_uid(char* uid_str);
int valid_mnemonic(char* str, char type);
int valid_pretty (char* str);
char* convert_to_hex(char* hex_str, unsigned char bytes[], int LEfile, int BEmachine);

int main(int argc, char *argv[]) {
  // All the flags that will be used in this program
  int mFlag = 0, iFlag = 0, oFlag = 0, stdoutFlag = 0;
  int opt;
  char BIN_FILE[FILE_BUFFER]; // should be a binary file containing .text section
  char OUTPUT_FILE[FILE_BUFFER]; // can be any file or "-" which specifies stdout
  char* INSTR_FILE = NULL;// file that contains the instruction mapping
  while ((opt = getopt(argc, argv, "m:i:o:h")) != -1) {
    switch (opt) {
      case 'h':
          USAGE(argv[0]);
          return EXIT_SUCCESS;
          break;
      case 'm':
          mFlag++;
          if ((INSTR_FILE = malloc(strlen(optarg)+1)) == NULL)
            return EXIT_FAILURE;
          strcpy(INSTR_FILE, optarg);
          break;
      case 'i':
          iFlag++;
          strcpy(BIN_FILE, optarg);
          break;
      case 'o':
          oFlag++;
          strcpy(OUTPUT_FILE, optarg);
          if (strcmp(OUTPUT_FILE, "-") == 0) { // "-" specifies stdout
            stdoutFlag++;
          }
          break;
      default:
      case '?':
          USAGE(argv[0]);
          return EXIT_FAILURE;
          break;
    }
  }

  // Display help and exit if no flags are provided
  if (!mFlag && !iFlag && !oFlag) {
    USAGE(argv[0]);
    return EXIT_FAILURE;
  }

  // Only one input of each flag should be provided
  if (mFlag > 1 || iFlag > 1 || oFlag > 1 || stdoutFlag > 1) {
    fprintf(stderr, "Bad usage.\nUse -h for more information.\n");
    return EXIT_FAILURE;
  }

  // The flags -i and -o are mandatory
  if (!iFlag || !oFlag) {
    fprintf(stderr, "Bad usage.\nUse -h for more information.\n");
    return EXIT_FAILURE;
  }

  // Program defaults to the file "instructions.txt" for instruction mapping
  // if -m is not provided
  if (!mFlag)
    INSTR_FILE = "instruction_mapping.txt";

  // Open files
  FILE* ifp = fopen(BIN_FILE, "rb");
  FILE* ofp = NULL;
  if (!stdoutFlag) // Only open an output file is output is not stdout
    ofp = fopen(OUTPUT_FILE, "w");
  FILE* instr_p = fopen(INSTR_FILE, "r");

  // Assure the files exist
  if (ifp == NULL || instr_p == NULL) {
    fprintf(stderr, "Could not open file.\n");
    close_files(ifp, ofp, instr_p, stdoutFlag, mFlag, INSTR_FILE, OUTPUT_FILE, 1);
    return EXIT_FAILURE;
  }

  // Assure no files are the same. If not, program exits.
  if (files_duplicate(BIN_FILE, OUTPUT_FILE, INSTR_FILE, stdoutFlag) == 0) {
    fprintf(stderr, "Cannot use the same file more than once.\n");
    close_files(ifp, ofp, instr_p, stdoutFlag, mFlag, INSTR_FILE, OUTPUT_FILE, 0);
    return EXIT_FAILURE;
  }

  // Assure the instruction file and the binary file are not empty. If not, program exits.
  if (files_empty(BIN_FILE, INSTR_FILE) == 0) {
    fprintf(stderr, "Cannot read an empty file.\n");
    close_files(ifp, ofp, instr_p, stdoutFlag, mFlag, INSTR_FILE, OUTPUT_FILE, 1);
    return EXIT_FAILURE;
  }

  // Assure the binary file is big-endian encoded. If not, program exits
  int LEfile = 0; // 1 if the input file is LE
  int bom = check_bom(ifp);
  if (bom == 0) {
    fprintf(stderr, "Invalid input file.\n");
    close_files(ifp, ofp, instr_p, stdoutFlag, mFlag, INSTR_FILE, OUTPUT_FILE, 1);
    return EXIT_FAILURE;
  }
  else if (bom == 1) {
    LEfile = 0;
  } else {
    LEfile = 1;
  }


  /*
  int BEmachine = 1;
  if (htonl(42) == 42) {
    BEmachine = 1;
  } else {
    BEmachine = 0;
  }
  */


  int BEmachine = 1;
  int test = 1;
  char* p = (char *)&test;
  if (p[0] == 1) {
  	BEmachine = 0;
  } else {
  	BEmachine = 1;
  }


  /*

  volatile uint32_t test = 0x01234567;
  if (*((uint8_t*)(&test)) == 0x67) {
    BEmachine = 0;
  } else {
    BEmachine = 1;
  }
  */

  // Assure the binary file has a multiple of 4 byte quantity. If not, program exits
  size_t byte_count = check_byte_count(BIN_FILE);
  if (byte_count == 0) {
    fprintf(stderr, "Invalid input file length.\n");
    close_files(ifp, ofp, instr_p, stdoutFlag, mFlag, INSTR_FILE, OUTPUT_FILE, 1);
    return EXIT_FAILURE;
  }

  // Begin by reading the instruction mapping file
  if (read_instr(instr_p, ofp, ifp, byte_count, stdoutFlag, LEfile, BEmachine) == 0) {
    fprintf(stderr, "Error reading instruction file.\n");
    close_files(ifp, ofp, instr_p, stdoutFlag, mFlag, INSTR_FILE, OUTPUT_FILE, 1);
    return EXIT_FAILURE;
  }

  // Close all the open files
  close_files(ifp, ofp, instr_p, stdoutFlag, mFlag, INSTR_FILE, OUTPUT_FILE, 0);

  return EXIT_SUCCESS;
}

/**
* Reads the instruction mapping file
* Type      Opcode
* J-type    000010 or 00011
* I-type    !000000 && !000010 && !000011
* R-type    000000
*/
int read_instr(FILE* instr, FILE* ofp, FILE* bin_file, size_t byte_count, int stdoutFlag, int LEfile, int BEmachine) {

  // Allocate space for the three InstrType nodes (one for each instruction type)
  InstrType* rType, *iType, *jType;
  if ((rType = malloc(sizeof(InstrType))) == NULL)
    return 0;

  if ((iType = malloc(sizeof(InstrType))) == NULL) {
    free(rType);
    rType = NULL;
    return 0;
  }

  if ((jType = malloc(sizeof(InstrType))) == NULL) {
    free(rType);
    free(iType);
    rType = NULL;
    iType = NULL;
    return 0;
  }

  // Initialize the list, with rType as the head
  initialize_list(rType, iType, jType);

/*
*
*             Begin reading the instruction file
*
*/
  // Iterate through instruction file line by line
  char instr_str[FILE_BUFFER]; // this will hold each line of the instr file

  char* read_instr = " ", *mnemonic = NULL, *hex_str = NULL, *uid_str = NULL;
  int freeFlag = 1, dontFreeM = 0;
  // Allocate space for a new node
  Instr* new_node = NULL;
  while ((read_instr = fgets(instr_str , FILE_BUFFER , instr)) != NULL) {

      // Getting the type
      char type = get_type(instr_str);

      // Check that type = i, j, or r
      if (type != 'j' && type != 'i' && type != 'r') {
        new_node = NULL;
        freeFlag = 0;
        mnemonic = NULL;
        free(uid_str);
        uid_str = NULL;
        goto failure;
      }

      if ((uid_str = malloc(9)) == NULL) {
        goto failure;
      }

      // Validate uid
      if (!valid_uid(&instr_str[2])) {
        freeFlag = 0;
        mnemonic = NULL;
        free(uid_str);
        uid_str = NULL;
        goto failure;
      }

      strncpy(uid_str, &instr_str[2], 8);
      *(uid_str+8) = '\0';

      // Validate the hex value is valid
      if (!valid_hex(uid_str)) {
        goto failure;
      }

      // Getting UID
      uint32_t uid = 0;
      uid = strtoul(uid_str, NULL, 16);
      errno = 0;
      if ((errno == ERANGE) || (errno != 0 && uid == 0)) {
        goto failure;
      }
      if (type == 'j' || type == 'i') {
        uid >>= 26;
      }
      free(uid_str);
      uid_str = NULL;

      /* Getting mnemonic */
      size_t m_length = get_mnemonic_length(instr_str);
      if ((mnemonic = malloc(m_length+1)) == NULL) {
        goto failure;
      }
      if ((mnemonic = get_mnemonic(mnemonic, instr_str, m_length)) == NULL) {
        goto failure;
      }
      // Validate the mnemonic is a string
      if (!valid_mnemonic(mnemonic, type)) {
        new_node = NULL;
        goto failure;
      }

      // Validate the length of the line is correct
      if(strlen(read_instr) != (13 + m_length + 1)) {
        new_node = NULL;
        goto failure;
      }

      // Getting pretty
      errno = 0;
      char* pretty_str = &instr_str[12+m_length];
      if (valid_pretty(pretty_str) == 0) {
        goto failure;
      }
      uint32_t pretty = strtoul(pretty_str, NULL, 16);
      if ((errno == ERANGE) || (errno != 0 && pretty == 0) || (pretty > 9)) {
        goto failure;
      }

      // Initialize the new node
      if ((new_node = malloc(sizeof(Instr))) == NULL) {
        goto failure;
      }
      new_node->uid = uid;
      new_node->pretty = pretty;
      new_node->mnemonic = mnemonic;

      // Allocate a new Instr struct for the r instruction
      if (type == 'r') {
        rType->count = rType->count + 1;
        if (list_insert(rType, new_node, mnemonic) == 0) {
          freeFlag = 0;
          goto failure;
        }
      }

      // Allocate a new Instr struct for the I instruction
      if (type == 'i') {
        iType->count = iType->count + 1;
        if (list_insert(iType, new_node, mnemonic) == 0) {
          freeFlag = 0;
          goto failure;
        }
      }

      // Allocate a new Instr struct for the J instruction
      if (type == 'j') {
        jType->count = jType->count + 1;
        if (list_insert(jType, new_node, mnemonic) == 0) {
          freeFlag = 0;
          goto failure;
        }
      }
    } // END READING OF INSTRUCTION FILE

    /* Check for error when reading instruction file */
    if (read_instr == NULL && !feof(instr)) {
      freeFlag = 0;
      goto failure;
    }
    if (ferror(instr)) {
      freeFlag = 0;
      goto failure;
    }

    // Print all the data of this list if -DCSE220 flag was provided during compilation
    struct_info(rType);

/*
*
*             Begin reading the binary (input) file
*
*/

    unsigned char bytes[BUFFER_SIZE];
    if ((hex_str = malloc(9)) == NULL) { // this will hold the hex representation of the bytes
      freeFlag = 0;
      goto failure;
    }

    size_t total_bytes = 0;
    size_t read_bytes;
    while ((read_bytes = fread(bytes, sizeof(unsigned char), BUFFER_SIZE, bin_file)) == BUFFER_SIZE) {

      total_bytes += read_bytes;

      // Convert the string to a hex value
      if ((hex_str = convert_to_hex(hex_str, bytes, LEfile, BEmachine)) == NULL) {
        freeFlag = 0;
        goto failure;
      }

      // Convert the hex string so that we may perform calculations on it
      errno = 0;
      unsigned int num;
      num = strtoul(hex_str, NULL, 16);
      if ((errno == ERANGE) || (errno != 0 && num == 0)) {
        freeFlag = 0;
        goto failure;
      }

      // Getting the opcode
      unsigned int opcode = ((num >> 26) & 0x3f);

      /* Reading an R-Type */
      if (opcode == 0) {
        if (write_rType(num, opcode, stdoutFlag, rType, rType->head, ofp) == 0) {
          freeFlag = 0;
          goto failure;
        }
      }

      /* Reading an I-Type */
      if (opcode != 0 && opcode != 2 && opcode != 3) {
        if (write_iType(num, opcode, stdoutFlag, rType, iType->head, ofp) == 0) {
          freeFlag = 0;
          goto failure;
        }
      }

      /* Reading a J-Type */
      if (opcode == 2 || opcode == 3) {
        if (write_jType(num, opcode, stdoutFlag, rType, jType->head, ofp) == 0) {
          freeFlag = 0;
          goto failure;
        }
      }
    }

    /* Check for error when reading instruction file */
    if (read_bytes != BUFFER_SIZE && !feof(bin_file)) {
      freeFlag = 0;
      goto failure;
    }

    if (ferror(instr)) {
      freeFlag = 0;
      goto failure;
    }

    if (total_bytes != (byte_count-4)) { // substract 4 because we started reading after the BOM
      freeFlag = 0;
      goto failure;
    }

    free(hex_str);
    hex_str = NULL;

    // Free the memory allocated to each of the InstrType nodes
    free_list(rType);

    goto success;

    failure:
      struct_info(rType);
      if (hex_str != NULL) {
        free(hex_str);
        hex_str = NULL;
      }
      if (mnemonic != NULL && freeFlag && !dontFreeM) {
        free(mnemonic);
        mnemonic = NULL;
      }
      if (uid_str != NULL) {
        free(uid_str);
        uid_str = NULL;
      }
      if (new_node != NULL && freeFlag) {
        free(new_node);
        new_node = NULL;
      }
      free_list(rType);

      return 0;

    success:
      return 1;
}

/**
* Validate pretty is in the range [0-9]
* @return 1 If pretty is valid, else 0
*/
int valid_pretty (char* str) {
  int i = 0;
  while (*(str+i) != '\r'&& *(str+i) != '\n' && *(str+i) != '\0' && i < 3) {
    i++;
  }

  if (i != 1) {
    return 0;
  }

  if (*str < 48 || *str > 57) {
    return 0;
  }

  return 1;
}

/**
* Validate the mnemonic is in the range [A-z]
* @return 1 If mnemonic is a string, else 0
*/
int valid_mnemonic(char* str, char type) {
  int i = 0;
  while (*(str+i) != '\0') {
    char c = *(str+i);
    if (c <= 64 || (c >= 91 && c <= 96) || (c >= 123))
      return 0;
    if (c >= 65 && c <= 90) // change all uppercase to lowercase
      *(str+i) = c + 32;
    i++;
  }

  return 1;
}


int valid_uid(char* uid_str) {
  int i = 0;
  while (*(uid_str+i) != ' ' && *(uid_str+i) != '\0' && i < 9) {
    i++;
  }

  if (i != 8)
    return 0;

  return 1;
}

/* Validates the string argument is in form 0xHEX_VALUE and each value
after 'x' is within the range [0123456789abcedfABCDEF]
Return 0 if invalid hex
Return 1 if valid hex */
int valid_hex(char* hex_str) {
  int length = strlen(hex_str);
  if (length < 8) {
    return 0;
  }

  int i;
  for (i = 0; i < length; i++) { // validate that each character is in range [0123456789abcedfABCDEF]
    if (hex_str[i] < 48 || ((hex_str[i] > 57) && (hex_str[i] < 65)) ||
      ((hex_str[i] > 70) && (hex_str[i] < 97)) || hex_str[i] > 102) {
        return 0;
    }
  }

  return 1;
}

int write_rType(unsigned int num, unsigned int opcode, int stdoutFlag, InstrType* head, Instr* cursor, FILE* ofp) {
  int rs = num >> 21;
  int rt = (num & 0x001F0000) >> 16;
  int rd = (num & 0x0000F800) >> 11;
  int shamt = (num & 0x000007C0) >> 6;
  int identifier = num & 0x3F; // tells what kind of instruction this is

  while (cursor != NULL) {
    if (cursor->uid == identifier) {
      if (stdoutFlag) { // write to stdout
        if (pretty_print(stdout, cursor, 0, shamt, rd, rs, rt) < 0) {
          return 0;
        }
      }
      else { // write to file
        if (pretty_print(ofp, cursor, 0, shamt, rd, rs, rt) < 0) {
          return 0;
        }
      }
    }
    cursor = cursor->next;
  }

  return 1;

}

int write_iType(unsigned int num, unsigned int opcode, int stdoutFlag, InstrType* head, Instr* cursor, FILE* ofp) {
  int imm = num & 0x0000FFFF;
  int rs = (num & 0x03E00000) >> 21;
  int rt = (num & 0x001F0000) >> 16;

  while (cursor != NULL) {
    if (cursor->uid == opcode) {
      if (stdoutFlag) { // write to stdout
        if (pretty_print(stdout, cursor, imm, 0, 0, rs, rt) < 0)
          return 0;
      }
      else { // write to file
        if (pretty_print(ofp, cursor, imm, 0, 0, rs, rt) < 0)
          return 0;
      }
    }
    cursor = cursor->next;
  }

  return 1;
}

int write_jType(unsigned int num, unsigned int opcode, int stdoutFlag, InstrType* head, Instr* cursor, FILE* ofp) {
  int imm = num & 0x03FFFFFF;

  while (cursor != NULL) {
    if (cursor->uid == opcode) {
      if(stdoutFlag) { // write to stdout
        if (pretty_print(stdout, cursor, imm, 0, 0, 0, 0) < 0)
          return 0;
      }
      else { // write to file
        if (pretty_print(ofp, cursor, imm, 0, 0, 0, 0) < 0)
          return 0;
      }
    }
    cursor = cursor->next;
  }

  return 1;
}

/**
* Returns the number of bytes written
*/
int pretty_print(FILE* file, Instr* node,  int imm, int shamt, int rd, int rs, int rt) {
  char* rd_str = get_reg_name(rd);
  char* rs_str = get_reg_name(rs);
  char* rt_str = get_reg_name(rt);

  if (rd_str == NULL || rs_str == NULL || rt_str == NULL) {
    return -1;
  }

  if (node->pretty == LOOK_1_REG) {
    return fprintf(file, "%s %s\n", node->mnemonic, rd_str);
  } else if (node->pretty == LOOK_2_REG) {
      return fprintf(file, "%s %s, %s\n", node->mnemonic, rs_str, rt_str);
  } else if (node->pretty == LOOK_2_REG_IMMED) {
      return fprintf(file, "%s %s, %s, 0x%x\n", node->mnemonic, rt_str, rs_str, imm);
  } else if (node->pretty == LOOK_3_REG) {
      return fprintf(file, "%s %s, %s, %s\n", node->mnemonic, rd_str, rt_str, rs_str);
  } else if (node->pretty == LOOK_MEM) {
      return fprintf(file, "%s %s, 0x%x(%s)\n", node->mnemonic, rt_str, imm, rs_str);
  } else if (node->pretty == LOOK_SYSCALL) {
      return fprintf(file, "%s\n", node->mnemonic);
  } else if (node->pretty == LOOK_JUMP) {
      return fprintf(file, "%s 0x%x\n", node->mnemonic, imm);
  } else if (node->pretty == LOOK_1_REG_IMMED) {
      return fprintf(file, "%s %s, 0x%x\n", node->mnemonic, rs_str, imm);
  } else if (node->pretty == LOOK_SHIFT) {
      return fprintf(file, "%s %s, %s, 0x%x\n", node->mnemonic, rd_str, rs_str, shamt);
  } else if (node->pretty == LOOK_BRANCH) {
      return fprintf(file, "%s %s, %s, 0x%x\n", node->mnemonic, rs_str, rt_str, imm);
  }

  return -1;
}

/*
*  TODO: Remove this hardcoding
*/
char* get_reg_name(int numeric) {
  switch (numeric) {
    case 0:
           return "$zero";
    case 1:
           return "$at";
    case 2:
           return "$v0";
    case 3:
           return "$v1";
    case 4:
          return "$a0";
    case 5:
          return "$a1";
    case 6:
          return "$a2";
    case 7:
          return "$a3";
    case 8:
          return "$t0";
    case 9:
          return "$t1";
    case 10:
          return "$t2";
    case 11:
          return "$t3";
    case 12:
          return "$t4";
    case 13:
          return "$t5";
    case 14:
          return "$t6";
    case 15:
          return "$t7";
    case 16:
          return "$s0";
    case 17:
          return "$s1";
    case 18:
          return "$s2";
    case 19:
          return "$s3";
    case 20:
          return "$s4";
    case 21:
          return "$s5";
    case 22:
          return "$s6";
    case 23:
          return "$s7";
    case 24:
          return "$t8";
    case 25:
          return "$t9";
    case 26:
          return "$k0";
    case 27:
          return "$k1";
    case 28:
          return "$gp";
    case 29:
          return "$sp";
    case 30:
          return "$fp";
    case 31:
          return "$ra";
  }
  return NULL;
}

/**
* Takes a pointer and an array of bytes, and concatenates each
* byte element into hex_str to create a single hex value in
* the format 0x12345678
* @return The new hexadecimal string if successful, else return NULL
*/
char* convert_to_hex(char* hex_str, unsigned char bytes[], int LEfile, int BEmachine) {
  int bytes_read = 0, total_read = 0;

  if (!BEmachine) {
  	if ((LEfile && !BEmachine)) {
      	unsigned char temp = bytes[3];
      	bytes[3] = bytes[0];
      	bytes[0] = temp;

     	 temp = bytes[2];
     	 bytes[2] = bytes[1];
      	bytes[1] = temp;
  	}
  } else {
    if ((LEfile && BEmachine)) {
        unsigned char temp = bytes[3];
        bytes[3] = bytes[0];
        bytes[0] = temp;

        temp = bytes[2];
        bytes[2] = bytes[1];
        bytes[1] = temp;
    }
  }

  if ((bytes_read = sprintf(hex_str+0, "%02x", (unsigned int)bytes[0] & 0xff)) < 0) {
        return NULL;
    } else {
      total_read += bytes_read;
  }


      if ((bytes_read = sprintf(hex_str+2, "%02x", (unsigned int)bytes[1] & 0xff)) < 0) {
        return NULL;
      } else {
        total_read += bytes_read;
      }

      if ((bytes_read = sprintf(hex_str+4, "%02x", (unsigned int)bytes[2] & 0xff)) < 0) {
        return NULL;
      } else {
        total_read += bytes_read;
    if ((bytes_read = sprintf(hex_str+6, "%02x", (unsigned int)bytes[3] & 0xff)) < 0) {
      return NULL;
    } else {
      total_read += bytes_read;
    }

  }

  if (total_read != 8)
    return NULL;

  return hex_str;
}

/**
* Insert the new node into the given list in sorted order
* List is sorted alphabetically inascending order
* Ex. add comes before addu and mul comes before sub
* @param list The InstrType to add this node into
* @param new_node The node to add to list
* @param head The head reference of list
* @param mnemonic The data of the node which determines where the node
*         is inserted in the list
* @return 1 if new_node was successfully added, else 0
*/
int list_insert(InstrType* list, Instr* new_node, char* mnemonic) {

  Instr* cursor = list->head;
  // Create the head of the rType struct if there is no current head
  if (list->head == NULL) {
    list->head = new_node;
    new_node->prev = NULL;
    new_node->next = NULL;
  } else { // There is already a head
      cursor = list->head;
      // Iterate through the list until the end
      if (cursor->next == NULL) {
        if (strcmp(mnemonic, cursor->mnemonic) < 0) { // replace head with new node
          list->head = new_node;
          cursor->prev = new_node;
          new_node->next = cursor;
          new_node->prev = NULL;
        } else {
            new_node->next = NULL;
            new_node->prev = cursor;
            cursor->next = new_node;
          }
      } else { // there is more than one element in this list
          while(cursor->next != NULL && strcmp(cursor->next->mnemonic, mnemonic) < 0) {
            cursor = cursor->next;
          }
          new_node->prev = cursor; // cursor is now at insertion point
          new_node->next = cursor->next;
          cursor->next = new_node;
        }
     }

  return 1;
}

/**
* Prints the list of isntructions
* This function is only called when the
* -DCSE220 flag is provided during compilation.
*/
void print_list(InstrType* cursor) {
  Instr* node = cursor->head;

  printf("\nCSE220: R-Type List:\n");
  while (node != NULL) {
    printf("CSE220: 0x%p UID: %d Pretty: %d Mnemonic: %s Next: 0x%p Prev: 0x%p\n", \
          node, node->uid, node->pretty, node->mnemonic, node->next, node->prev);
    node = node->next;
  }

  node = cursor->next->head;
  printf("\nCSE220: I-Type List:\n");
  while (node != NULL) {
    printf("CSE220: 0x%p UID: %d Pretty: %d Mnemonic: %s Next: 0x%p Prev: 0x%p\n", \
          node, node->uid, node->pretty, node->mnemonic, node->next, node->prev);
    node = node->next;
  }

  node = cursor->next->next->head;
  printf("\nCSE220: J-Type List:\n");
  while (node != NULL) {
    printf("CSE220: 0x%p UID: %d Pretty: %d Mnemonic: %s Next: 0x%p Prev: 0x%p\n", \
          node, node->uid, node->pretty, node->mnemonic, node->next, node->prev);
    node = node->next;
  }
  printf("\n");
}

int get_mnemonic_length(char instr_str[]) {
  char* ptr = instr_str + 11; // point ptr to the beginning of the mnemonic
  int m_length = 0;
  while (*(ptr+m_length) != ' ' && *(ptr+m_length) != '\0') {
    m_length++;
  }

  return m_length;
}

char* get_mnemonic(char* mnemonic, char instr_str[], int m_length) {

  strncpy(mnemonic, &instr_str[11], m_length);
  *(mnemonic+m_length) = '\0';

  return mnemonic;
}

char get_type(char instr_str[]) {
  char type;
  type = instr_str[0];

  return type;
}

/**
* Initialize each type's data, and initialize the doubly linked list
* Structure is [NULL <- rType <-> iType <-> jType -> NULL]
* rType is the head of this list.
*/
void initialize_list(InstrType* head, InstrType* iType, InstrType* jType) {

  // Initialize type
  head->type = 'R';
  iType->type = 'I';
  jType->type = 'J';

  // Initialize count
  head->count = 0;
  iType->count = 0;
  jType->count = 0;

  // Initialize the InstrType node after this one
  head->next = iType;
  iType->next = jType;
  jType->next = NULL;

  // Initalize the InstrType node before this one
  head->prev = NULL;
  iType-> prev = head;
  jType-> prev = iType;

  // Initialize the first Instr node of this type
  head->head = NULL;
  iType->head = NULL;
  jType->head = NULL;
}

/**
* Frees this doubly linked list
* The head of the list (rType) must be passed in as head.
*/
void free_list(InstrType* head) {

  if (head == NULL)
    return;

  size_t r_count = head->count;
  size_t i_count = head->next->count;
  size_t j_count = head->next->next->count;
  size_t total_nodes = r_count + i_count + j_count;
  size_t mnemonic_mem = 0; // holds how much all the mnemonic's take up in memory

  InstrType* cursor = head;
  while (cursor != NULL) {
    Instr* instr_cursor = cursor->head;

    // Free all of the instructions of this instruction type
    while (instr_cursor != NULL) {
      // Free the mnemonic
      mnemonic_mem += strlen(instr_cursor->mnemonic)+1;
      free(instr_cursor->mnemonic);
      instr_cursor->mnemonic = NULL;

      Instr* next = instr_cursor->next;
      free(instr_cursor); // free this node
      instr_cursor = next;
    }

    InstrType* next = cursor->next;
    free(instr_cursor);
    instr_cursor = NULL;
    free(cursor);
    cursor = next;
  }
  free(cursor);
  cursor = NULL;

  size_t total_mem = ((3 * sizeof(InstrType)) + (total_nodes * sizeof(Instr)) + mnemonic_mem);
  total_mem++; total_mem--; // to please the compiler

  // Only printed when -DCSE220 flag is provided

  memory_used(r_count, i_count, j_count, sizeof(InstrType), sizeof(Instr), total_mem);
}

/**
* Checks to see that the binary file has a 4 byte quantity.
* If not, the program exits.
*/
off_t check_byte_count(char* binFile) {

   struct stat buffer;
   if (stat(binFile, &buffer) != 0)
     return 0;

   if (buffer.st_size % 4 != 0)
     return 0;

   return buffer.st_size;
}

/**
* Checks to see that the byte order mark of the binary(input) file
* is big-endian (0x576F6E67).
* If not, the program exists.
* @return 1 if the file is big-endian
* @return 2 if the file is little-endian
* @return 0 if the file is neither
*/
int check_bom(FILE* binFile) {
  unsigned char bom[BUFFER_SIZE];
  if (fread(bom, sizeof(unsigned char), BUFFER_SIZE, binFile) != BUFFER_SIZE) {
    return 0;
  }

  if (ferror(binFile)) {
    return 0;
  }

  if ((bom[0] == 87) && (bom[1] == 111) && (bom[2] == 110) && (bom[3] == 103)) {// BE
    return 1;
  }

  if ((bom[0] == 103) && (bom[1] == 111) && (bom[2] == 110) && (bom[3] == 87)) { // LE
    return 2;
  }

  return 0;
}

/**
* Checks to see that all given files were successfully open
* If not, the program exits.
* stdoutFlag == 1 if output is to stdout
*/
int file_success(FILE* input, FILE* output, FILE* instr, int stdoutFlag) {
  if (!stdoutFlag) {
    if (input == NULL || output == NULL || instr == NULL) {
      fprintf(stderr, "Could not open file.\n");
      return 0;
    }
  } else {
      if (input == NULL || instr == NULL) {
        fprintf(stderr, "Could not open file.\n");
        return 0;
      }
  }

  return 1;
}

/**
* Closes the input, output, and instruction mapping files
* as long as they exist
* stdoutFlag == 1 if output is to stdout
*/
void close_files(FILE* ifp, FILE* ofp, FILE* instr_p, int stdoutFlag, int mFlag, char* INPUT_FILE, char* OUTPUT_FILE, int deleteOutFlag) {

  if (mFlag) {
    free(INPUT_FILE);
    INPUT_FILE = NULL;
  }

  if (ifp != NULL) {
    fclose(ifp);
    ifp = NULL;
  }

  if (!stdoutFlag && ofp != NULL) {
    fclose(ofp);
    if (deleteOutFlag) {
      unlink(OUTPUT_FILE);
    }
    ofp = NULL;
  }

  if (instr_p != NULL) {
    fclose(instr_p);
    instr_p = NULL;
  }
}

/**
* Checks to see that none of the files are equivalent to another
* If any are duplicates, then the program exits.
* stdoutFlag == 1 if output is to stdout
*/
int files_duplicate(char* input, char* output, char* instr, int stdoutFlag) {
  struct stat ibuffer, obuffer, instr_buf;

  // stat() returns 0 if it was unsuccessful
  if (!stdoutFlag) {
    if ((stat(input, &ibuffer) != 0) || (stat(instr, &instr_buf) != 0) || (stat(output, &obuffer) != 0)) {
      return 0;
    }
  } else {
    if ((stat(input, &ibuffer) != 0) || (stat(instr, &instr_buf) != 0)) {
      return 0;
    }
  }


  // If any of the inodes are equal, then a duplicate file has been used
  if (!stdoutFlag) {
    if((instr_buf.st_ino == obuffer.st_ino) || (instr_buf.st_ino == ibuffer.st_ino) ||
      (ibuffer.st_ino == obuffer.st_ino)) {
        return 0;
    }
  } else {
      if(instr_buf.st_ino == ibuffer.st_ino) {
          return 0;
      }
    }

  return 1;
}

/**
* Check to see that none of the files are empty
* If any are empty, then the program exits
*/
int files_empty(char* input, char* instr) {
  struct stat ibuffer, instr_buf;
  if ((stat(input, &ibuffer) != 0) || (stat(instr, &instr_buf) != 0))
    return 0;

  if (!instr_buf.st_size || !ibuffer.st_size)
    return 0;

  return 1;
}
