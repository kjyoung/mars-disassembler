# README #

# Description #

A program that can disassemble a Mars binary text section back into human readable assembly language.

This was an assignment for CSE 220 - Systems Level Programming at SBU, done during my Fall 2014 semester.